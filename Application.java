public class Application {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("A text file was not provided");
            System.exit(1);
        }

        String fileName = args[0];

        AlphabetSoup alphabetSoup = new AlphabetSoup(fileName);
        alphabetSoup.startSolution();
    }
}
