import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class AlphabetSoup {
    private String fileName;
    private static int[][] DIRECTIONS;

    public AlphabetSoup(String fileName) {
        this.fileName = fileName;
    }

    static {
        DIRECTIONS = new int[][] {
                { 0, 1 },
                { 1, 1 },
                { 1, 0 },
                { 1, -1 },
                { 0, -1 },
                { -1, -1 },
                { -1, 0 },
                { -1, 1 }
        };
    }

    public void startSolution() {
        List<String> textLines;
        try {
            textLines = Files.readAllLines(Paths.get(fileName));
            int[] dimensions = getDimensions(textLines.get(0));

            int rowCount = dimensions[0];
            char[][] board = new char[rowCount][];

            for(int i = 0; i < board.length; i++) {
                // increment by 1 because first line of textLines contains the dimensions
                board[i] = textLines.get(i + 1).replaceAll("\\s", "").toCharArray();
            }

            for(int i = 1 + rowCount; i < textLines.size(); i++) {
                if(textLines.get(i).equals("")) {
                    continue;
                }
                String foundText = searchBoard(board, textLines.get(i));
                if(foundText != null) {
                    System.out.println(foundText);
                }
            }

        } catch (IOException e) {
            System.err.println("Error reading file: " + e.getMessage());
        }
    }

    private String searchBoard(char[][] board , String word) {
        for(int row = 0; row < board.length; row++) {
            for(int column = 0; column < board[0].length; column++) {
                for(int[] direction : DIRECTIONS) {
                    int[] endCoordinates = searchWord(board, word, row, column, direction);

                    if(endCoordinates != null) {
                        return String.format("%s %d:%d %d:%d", word, row, column, endCoordinates[0], endCoordinates[1]);
                    }
                }
            }
        }
        return null;
    }

    private int[] searchWord(char[][] board , String word, int row, int col, int[] direction) {
        int i = 0;
        while(i < word.length() && row >= 0 && row <= board.length - 1 && col >= 0 && col <= board[0].length - 1) {
            if(word.charAt(i) == board[row][col]) {
                i++;
                if(i == word.length()) {
                    return new int[] {row, col};
                } else {
                    row += direction[0];
                    col += direction[1];
                }
            } else {
                break;
            }
        }

        return null;
    }

    private int[] getDimensions(String dimension) {
        String[] dimensionNumStrings = dimension.split("x");
        return new int[] {Integer.valueOf(dimensionNumStrings[0]), Integer.valueOf(dimensionNumStrings[1])};
    }
}
